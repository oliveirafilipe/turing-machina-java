package validator;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextArea;
import validator.Main;
import view.TextLineNumber;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JEditorPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class TelaEntrada extends JFrame {

	private JPanel contentPane;
	private JButton btnDoTuring;
	private JTextPane inputTextPane;
	private JTextPane outputTextPane;
	private JTabbedPane tabbedPane;
	
	
	public JTextPane getInputTextPane() {
		return inputTextPane;
	}

	public JButton getBtnDoTuring() {
		return btnDoTuring;
	}

	public JTextPane getOutputTextPane() {
		return outputTextPane;
	}

	public void setBtnDoTuring(JButton btnDoTuring) {
		this.btnDoTuring = btnDoTuring;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaEntrada frame = new TelaEntrada();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaEntrada() {
		setTitle("MÁQUINA DE TURING");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 424, 435);
		setLocationRelativeTo(null);
		
		tabbedPane = new JTabbedPane();
		
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		btnDoTuring = new JButton("Ir");
		btnDoTuring.setFont(new Font("Dialog", Font.BOLD, 15));
		panel.add(btnDoTuring);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setFont(new Font("Dialog", Font.BOLD, 15));
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				inputTextPane.setText("");
			}
		});
		panel.add(btnLimpar);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		
		JLabel lblMquinaDeTuring = new JLabel("Máquina de Turing");
		lblMquinaDeTuring.setFont(new Font("Dialog", Font.BOLD, 12));
		panel_1.add(lblMquinaDeTuring);
		
		inputTextPane = new JTextPane();
		inputTextPane.setFont(new Font("Courier New", Font.PLAIN, 20));
		JScrollPane scrollPane = new JScrollPane(inputTextPane);
		TextLineNumber tln = new TextLineNumber(inputTextPane);
		scrollPane.setRowHeaderView(tln);
		inputTextPane.setText(placeHolder);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		tabbedPane.add("Input", contentPane);
		tabbedPane.add("Output", this.getInputPanel());
		this.setContentPane(tabbedPane);
		
	}
	
	private JPanel getInputPanel() {
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel.setLayout(new BorderLayout(0, 0));
		
		outputTextPane = new JTextPane();
		outputTextPane.setFont(new Font("Courier New", Font.PLAIN, 20));
		JScrollPane scrollPane = new JScrollPane(outputTextPane);
		TextLineNumber tln = new TextLineNumber(outputTextPane);
		scrollPane.setRowHeaderView(tln);
		panel.add(scrollPane, BorderLayout.CENTER);
		return panel;
	}
	
	public void activeOutputPane() {
		this.tabbedPane.setSelectedIndex(1);
	}

	private final String placeHolder = 
			"^q0:\n" +
			"a,A,D,q1\n" +
			"B,B,D,q3\n" +
			"q1:\n" +
			"a,a,D,q1\n" +
			"B,B,D,q1\n" +
			"b,B,E,q2\n" +
			"q2:\n" +
			"a,a,E,q2\n" +
			"B,B,E,q2\n" +
			"A,A,D,q0\n" +
			"*q3:\n" +
			"B,B,D,q3\n";

}
