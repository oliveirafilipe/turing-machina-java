package validator;

public class RawInstruction {
	private char side;
	private char symbolToInsert;
	private char symbolToSearch;
	private String nextState;
	
	public RawInstruction(char side, char symbolToInsert, char symbolToSearch, String nextState) {
		super();
		this.side = side;
		this.symbolToInsert = symbolToInsert;
		this.symbolToSearch = symbolToSearch;
		this.nextState = nextState;
	}

	public char getSide() {
		return side;
	}

	public void setSide(char side) {
		this.side = side;
	}

	public char getSymbolToInsert() {
		return symbolToInsert;
	}

	public void setSymbolToInsert(char symbolToInsert) {
		this.symbolToInsert = symbolToInsert;
	}

	public char getSymbolToSearch() {
		return symbolToSearch;
	}

	public void setSymbolToSearch(char symbolToSearch) {
		this.symbolToSearch = symbolToSearch;
	}

	public String getNextState() {
		return nextState;
	}

	public void setNextState(String nextState) {
		this.nextState = nextState;
	}
	
}
