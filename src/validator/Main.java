package validator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import turing.Instruction;
import turing.State;
import turing.Turing;
import validator.RawInstruction;
import validator.RawState;
import validator.Validator;
import view.InfoDialog;

public class Main implements ActionListener {
	private TelaEntrada tela;
	private Turing turing;
	
	public void Ir(){
		try {
			
			ArrayList<String> lines = new ArrayList<>();
			String s[] = this.tela.getInputTextPane().getText().split("\\r?\\n");
			for(int i = 0; i<s.length; i++) 
			{
					System.out.println(s[i]);
					lines.add(s[i]);
			}		
			// Call the Validations
			ArrayList<RawState> rawStates = Validator.validate(lines);
			
			ArrayList<State> turingStates = new ArrayList<State>();
			
			String input = JOptionPane.showInputDialog("Por favor digite a sequência de entrada. (Ex.: aaabccd)");
			Turing turing = new Turing(input);//"aaabccd"

			for (Iterator iterator = rawStates.iterator(); iterator.hasNext();) {
				RawState rawState = (RawState) iterator.next();
				turingStates.add(new State(rawState.getName(), rawState.isFinal(), rawState.isInitial()));
			}

			for (int i = 0; i < rawStates.size(); i++) {
				RawState rawState = rawStates.get(i);
				ArrayList<Instruction> instructions = new ArrayList<Instruction>();
				for (Iterator iterator2 = rawState.getInstructions().iterator(); iterator2.hasNext();) {
					RawInstruction instruction = (RawInstruction) iterator2.next();
					for (Iterator iterator3 = turingStates.iterator(); iterator3.hasNext();) {
						State state = (State) iterator3.next();
						if (state.getName().equalsIgnoreCase(instruction.getNextState())) {
							instructions.add(new Instruction(instruction.getSide(), instruction.getSymbolToInsert(), instruction.getSymbolToSearch(), state));
						}
					}
				}
				turingStates.get(i).setInstructionList(instructions);
				turing.add(turingStates.get(i));
			}
			
			Object[] options = {"Entendi!"};
			int n = JOptionPane.showOptionDialog(null,
					"Aguarde. O Processamento pode demorar.",
			"Info",
			JOptionPane.OK_OPTION,
			JOptionPane.WARNING_MESSAGE,
			null,     //do not use a custom Icon
			options,  //the titles of buttons
			options[0]); //default button title

			if(turing.doTuring()) {
				System.out.println("Aceito");
				this.tela.getOutputTextPane().setText(this.turing.getResultado());
				this.tela.activeOutputPane();
				JOptionPane.showMessageDialog(null, "Programa Aceito!");
				
			} else {
				System.out.println("Recusado");
				this.tela.getOutputTextPane().setText(this.turing.getResultado());
				this.tela.activeOutputPane();
				JOptionPane.showMessageDialog(null, "Programa Recusado!");
			}
		} catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Ocorreu um erro:\n" + e.getMessage());
		}
	}

	public Main(TelaEntrada tela) {
		super();
		this.tela = tela;
		
		this.tela.getBtnDoTuring().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getActionCommand().equals("Ir")) {
			System.out.println("Resultado");
			Ir();
		}
		
	}
}
