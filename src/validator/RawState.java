package validator;

import java.util.ArrayList;

public class RawState {
	private String name;
	private boolean isFinal;
	private boolean isInitial;
	private ArrayList<RawInstruction> instructions = new ArrayList<RawInstruction>();

	public RawState(String name, boolean isFinal, boolean isInitial) {
		super();
		this.name = name;
		this.isFinal = isFinal;
		this.isInitial = isInitial;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isFinal() {
		return isFinal;
	}

	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}

	public boolean isInitial() {
		return isInitial;
	}

	public void setInicial(boolean isInicial) {
		this.isInitial = isInicial;
	}

	public ArrayList<RawInstruction> getInstructions() {
		return instructions;
	}

	public void setInstructions(ArrayList<RawInstruction> instructions) {
		this.instructions = instructions;
	}
	
	public void addInstruction(RawInstruction instruction) {
		this.instructions.add(instruction);
	}
	public boolean isInstructionsEmpty() {
		return this.instructions.size() == 0;
	}

}