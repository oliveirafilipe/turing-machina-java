package validator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	public static final String STATE_PATTERN = "(\\^|\\*)?([a-zA-Z0-9]*)\\:";
	public static final String INSTRUCTION_PATTERN = "([a-zA-Z]),([a-zA-Z]),(D|E),(\\w*)";

	public static ArrayList<RawState> validate(ArrayList<String> lines) {
		int lineNumber = 0;
		RawState actualState = null;
		ArrayList<RawState> states = new ArrayList<RawState>();
		for (Iterator iterator = lines.iterator(); iterator.hasNext();) {
			String line = (String) iterator.next();
			if (isState(line)) {
				actualState = createRawState(line);
				states.add(actualState);
			} else if (lineNumber == 0) {
				throw new Error("Primeira Linha deve Conter um Estado");
			} else if (isInstruction(line)) {
				actualState.addInstruction(createRawInstruction(line));
			} else {
				throw new Error("Erro na Linha " + (lineNumber + 1) + ": " + line);
			}
			lineNumber++;
		}
		System.out.println("Tudo Finalizado Correto!");
		return states;
	}

	private static boolean isInstruction(String line) {
		return line.matches(INSTRUCTION_PATTERN);
	}

	public static boolean isState(String line) {
		return line.matches(STATE_PATTERN);
	}

	private static RawState createRawState(String line) {
		Matcher matcher = Pattern.compile(STATE_PATTERN).matcher(line);
		if (matcher.find()) {
			boolean isFinal = false; 
				if (matcher.group(1) != null) {
					isFinal = matcher.group(1).equalsIgnoreCase("*");
				}
			boolean isInitial = false;
			if (matcher.group(1) != null) {
				isInitial = matcher.group(1).equalsIgnoreCase("^");
			}
			String name = matcher.group(2);
			return new RawState(name, isFinal, isInitial);
		}
		return null;
	}
	
	private static RawInstruction createRawInstruction(String line) {
		Matcher matcher = Pattern.compile(INSTRUCTION_PATTERN).matcher(line);
		if (matcher.find()) {
			return new RawInstruction(matcher.group(3).charAt(0), matcher.group(2).charAt(0), matcher.group(1).charAt(0), matcher.group(4));
		}
		return null;
	}
}