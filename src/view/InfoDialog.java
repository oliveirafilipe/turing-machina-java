package view;

import java.awt.Dialog;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class InfoDialog extends JDialog {
	
	public InfoDialog(String message) {
		super();
	    JPanel panel = new JPanel();
	    JLabel jlabel = new JLabel("This is a label");
	    // jlabel.setFont(new Font("Verdana",1,20));
	    panel.add(jlabel);
		//this.setContentPane(new JPanel());
		this.setContentPane(panel);
		//this.setContentPane(JOptionPane.showMessageDialog(null, "Aguarde. O Processamento pode demorar."));
		//this.setContentPane(new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null));
		this.setTitle("Message");
		//this.setModal(false);
		// this.setModalityType(Dialog.ModalityType.MODELESS);
		//this.setDefaultCloseOperation(this.DO_NOTHING_ON_CLOSE);
		this.pack();
	}
}
