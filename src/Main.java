import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import turing.Instruction;
import turing.State;
import turing.Turing;
import validator.RawInstruction;
import validator.RawState;
import validator.Validator;

public class Main {
	public static final String INSTRUCTION_PATTERN = "([a-zA-Z]),([a-zA-Z]),(D|E),(\\w*)";

	public static void main(String[] args) {
		try {
			//ArrayList<String> lines = getFromJTextArea();
			ArrayList<String> lines = getFromFile();

			// Call the Validations
			ArrayList<RawState> rawStates = Validator.validate(lines);
			
			ArrayList<State> turingStates = new ArrayList<State>();
			
			Turing turing = new Turing("aaabccd"+Turing.EMPTY_CHAR);

			for (Iterator iterator = rawStates.iterator(); iterator.hasNext();) {
				RawState rawState = (RawState) iterator.next();
				turingStates.add(new State(rawState.getName(), rawState.isFinal(), rawState.isInitial()));
			}

			for (int i = 0; i < rawStates.size(); i++) {
				RawState rawState = rawStates.get(i);
				ArrayList<Instruction> instructions = new ArrayList<Instruction>();
				for (Iterator iterator2 = rawState.getInstructions().iterator(); iterator2.hasNext();) {
					RawInstruction instruction = (RawInstruction) iterator2.next();
					for (Iterator iterator3 = turingStates.iterator(); iterator3.hasNext();) {
						State state = (State) iterator3.next();
						if (state.getName().equalsIgnoreCase(instruction.getNextState())) {
							instructions.add(new Instruction(instruction.getSide(), instruction.getSymbolToInsert(), instruction.getSymbolToSearch(), state));
						}
					}
				}
				turingStates.get(i).setInstructionList(instructions);
				turing.add(turingStates.get(i));
			}
			
			if(turing.doTuring()) {
				System.out.println("Aceito");
			} else {
				System.out.println("Recusado");
			}
		} catch (Error e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ArrayList<String> getFromFile() throws FileNotFoundException {
		File file = new File("/home/filipe-oliveira/eclipse-workspace/turing-machine/src/validator/input.txt");
		Scanner sc;
		ArrayList<String> lines = new ArrayList<String>();

		sc = new Scanner(file);
		while (sc.hasNextLine())
			lines.add(sc.nextLine());
		sc.close();
		return lines;

	}
	
//	public static ArrayList<String> getFromJTextArea() {
//	    String s[] = jTextArea1.getText().split("\\r?\\n");
//	    return new ArrayList<>(Arrays.asList(s));
//	}
}
