package turing;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import turing.Instruction;
import turing.State;

public class Main {
	public static void main(String[] args) {
		Turing turing = new Turing("abcba*");
		State step1 = new State("q0",false, true);
		State step2 = new State("q1", false, false);
		State step3 = new State("q2", false, false);
		State step4 = new State("q3", true, false);
		ArrayList<Instruction> instructionList = new ArrayList<Instruction>();
		instructionList.add(new Instruction('D', 'A', 'a', step2));
		instructionList.add(new Instruction('D', 'B', 'b', step2));
		instructionList.add(new Instruction('E', 'C', 'c', step2));
		step1.setInstructionList(instructionList);
		
		ArrayList<Instruction> instructionList2 = new ArrayList<Instruction>();
		instructionList2.add(new Instruction('D', 'B', 'b', step1));
		step2.setInstructionList(instructionList2);
		
		turing.add(step1);
		turing.add(step2);
		
		State states[] = new State[] {step1, step2};
		turing.doTuring();
	}
}
