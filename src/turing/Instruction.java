package turing;

public class Instruction {
	private char side;
	private char symbolToInsert;
	private char symbolToSearch;
	private State nextState;
	public Instruction(char side, char symbolToInsert, char symbolToSearch, State nextState) {
		super();
		this.side = side;
		this.symbolToInsert = symbolToInsert;
		this.symbolToSearch = symbolToSearch;
		this.nextState = nextState;
	}
	public char getSymbolToInsert() {
		return symbolToInsert;
	}
	public void setSymbolToInsert(char symbolToInsert) {
		this.symbolToInsert = symbolToInsert;
	}
	public char getSymbolToSearch() {
		return symbolToSearch;
	}
	public void setSymbolToSearch(char symbolToSearch) {
		this.symbolToSearch = symbolToSearch;
	}
	public State getNextState() {
		return nextState;
	}
	public void setNextState(State nextState) {
		this.nextState = nextState;
	}
	public char getSide() {
		return side;
	}
	public void setSide(char side) {
		this.side = side;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return symbolToSearch+","+symbolToInsert+","+side+"->"+nextState.getName();
	}

	
}
