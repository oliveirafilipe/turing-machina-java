package turing;

import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import validator.TelaEntrada;

public class Turing extends LinkedList<State> {
	public static final char EMPTY_CHAR = 'β';
	private String input;
	private static String resultado = "";
	
	public static String getResultado() {
		return resultado;
	}

	public static void setResultado(String resultado) {
		Turing.resultado = resultado;
	}

	public Turing(String input) {
		super();
		this.input = input + this.EMPTY_CHAR;
	}
	
	public boolean doTuring() {
		char[] inputAsArray = this.input.toCharArray();
		State actualState = this.getInitial();
		int step = 0;
		resultado = resultado + actualState.getName()+"\n";
		print(inputAsArray, step);
		while(true) {
			actualState.printInstructions();
			resultado = resultado + actualState.getName()+"\n";
			Instruction instruction = actualState.getInstructionForInput(inputAsArray[step]);
			if(instruction != null) {
				actualState = instruction.getNextState();
				inputAsArray[step] = instruction.getSymbolToInsert();
				if(instruction.getSide() == 'D')
					step++;
				else
					step--;
			} else {
				if (actualState.isFinal() && inputAsArray[step] == this.EMPTY_CHAR) {
					// System.out.println("ACEITO");
					return true;
				} else {
					// System.out.println("Travado");
					return false;
				}
			}
			print(inputAsArray, step);
		}
	}
	
	private State getInitial() {
		for (Iterator iterator = this.iterator(); iterator.hasNext();) {
			State state = (State) iterator.next();
			if (state.isInitial()) {
				return state;
			}
			
		}
		return this.getFirst();
	}

	private static void print(char[] input, int step) {
		for (int i = 0; i < input.length; i++) {
			System.out.print(input[i] + "|");
			resultado = resultado + input[i] + "|";
		}
		System.out.println();
		resultado = resultado + "\n";
		for (int i = 0; i < input.length; i++) {
			if (i == step) {
				System.out.print("^ ");
				resultado = resultado + "^";
			}else {
				System.out.print("  ");
				resultado = resultado + "  ";
			}
		}
		System.out.println();
		resultado = resultado + "\n";
	}
	
}
