package turing;
import java.util.ArrayList;
import java.util.Iterator;

public class State {
	private String name;
	private boolean isFinal;
	private boolean isInitial;
	private ArrayList<Instruction> instructionList = new ArrayList<Instruction>();

	public State(String name, boolean isFinal, boolean isInitial) {
		super();
		this.name = name;
		this.isFinal = isFinal;
		this.isInitial = isInitial;
	}

	public ArrayList<Instruction> getInstructionList() {
		return instructionList;
	}
	public void setInstructionList(ArrayList<Instruction> instructionList) {
		this.instructionList = instructionList;
	}
	
	public boolean isFinal() {
		return isFinal;
	}

	public void setFinal(boolean isFinal) {
		this.isFinal = isFinal;
	}

	public boolean isInitial() {
		return isInitial;
	}

	public void setInitial(boolean isInitial) {
		this.isInitial = isInitial;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Instruction getInstructionForInput(char input) {
		for (Iterator iterator = instructionList.iterator(); iterator.hasNext();) {
			Instruction instruction = (Instruction) iterator.next();
			if (instruction.getSymbolToSearch() == input) {
				return instruction;
			}
		}
		return null;
	}
	
	public void printInstructions() {
		System.out.println(this.name+":");
		for (Iterator iterator = instructionList.iterator(); iterator.hasNext();) {
			Instruction instruction = (Instruction) iterator.next();
			System.out.println("  "+instruction.toString());
		}
	}
	
}
